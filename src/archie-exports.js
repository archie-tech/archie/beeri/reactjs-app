const archieExports = {
  public_assets_bucket: "https://" + process.env.REACT_APP_PUBLIC_ASSETS_BUCKET,
  private_assets_bucket: "https://" + process.env.REACT_APP_PRIVATE_ASSETS_BUCKET,
  docs_endpoint: "https://" + process.env.REACT_APP_API_ENDPOINT + "/docs",
  beeri_ip: process.env.REACT_APP_BEERI_IP.split(',')
};

export default archieExports;
