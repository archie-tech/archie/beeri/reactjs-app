#!/usr/bin/env python3

import logging
import os
import json
import boto3


AWS_REGION = os.getenv("AWS_REGION", "eu-west-1")
AWS_SECRET = os.getenv("AWS_SECRET", "archie_beeri")


def configure_logger():
    logging_level = getattr(logging, os.getenv("logging_level", "INFO"))
    logging.basicConfig(format='%(levelname)s:%(module)s.%(funcName)s:%(message)s', level=logging_level, force=True)


def get_secret():
    logging.info("retriving values of secret {}".format(AWS_SECRET))
    client = boto3.client('secretsmanager', region_name=AWS_REGION)
    response = client.get_secret_value(SecretId=AWS_SECRET)
    secret = json.loads(response['SecretString'])
    return secret


def create_env_file(secret):
    with open(".env", 'w') as out_file:
        for k in secret.keys():
            out_file.write("REACT_APP_{}={}".format(k.upper(), secret[k]))
            out_file.write('\n')


if __name__ == "__main__":
    configure_logger()
    secret = get_secret()
    create_env_file(secret)
